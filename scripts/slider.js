var originImageW;
var originImageH;

function originImageLoad() {
	originImageW = $(".current-image").width();
	originImageH = $(".current-image").height();
}

function scaleNavLinks() {
	var ih = $(".current-image").height();
	var ch = $(".image-conteiner").height();
	
	$(".prev").height(ch);		
	$(".prev").css("margin-top", -ih);
	
	$(".next").height(ch);			
	$(".next").css("margin-top", -ih);
}

function scaleImage() {
	
	var cw = $(".image-conteiner").width();
	
	var iw = originImageW;
	
	var scaleW = iw / cw;
	
	var windowH = $(window).height();
	
	var ih = originImageH;
	
	var scaleH = ih / windowH;
	
	if(scaleW >= scaleH) {
		scaleImageWidth();
	} else {
		scaleImageHeight();
	}		
	
}

function scaleImageHeight() {
	var windowH = $(window).height() - 5;
	
	var ih = originImageH;
	
	if(ih > windowH) {
		$(".current-image").width("");
		$(".current-image").height(windowH);
	} else {
		$(".current-image").width("");
		$(".current-image").height(ih);
	}
	
	var cw = $(".image-conteiner").width();
	
	var iw = $(".current-image").width();
	
	var im = Math.floor((cw - iw) / 2);						
	$(".current-image").css("margin-left",im);
	
}

function scaleImageWidth() {
	var cw = $(".image-conteiner").width();
	
	var iw = originImageW;
	
	if(iw > cw) {
		$(".current-image").height("");
		$(".current-image").width(cw);
		$(".current-image").css("margin-left","");
	} else {
		$(".current-image").height("");
		$(".current-image").width(iw);
		var im = Math.floor((cw - iw) / 2);						
		$(".current-image").css("margin-left",im);
	}
}



$(document).ready(function() {
	$(".current-image").load(function() {
			
			originImageLoad();
			 
			scaleImage();
			
			scaleNavLinks();
			
	});
	$(window).resize(function() {
		scaleImage(); 
		scaleNavLinks();
	});
	
}
);